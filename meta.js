module.exports = {
    "helpers": {
        "if_or": function (v1, v2, options) {
            if (v1 || v2) {
                return options.fn(this);
            }
            return options.inverse(this);
        }
    },
    "prompts": {
        "name": {
            "type": "input",
            "required": true,
            "message": "Project name"
        },
        "description": {
            "type": "input",
            "required": false,
            "message": "Project description",
            "default": "A React project"
        },
        "author": {
            "type": "input",
            "message": "Author"
        },
        "lint": {
            "type": "confirm",
            "message": "Use ESLint to lint your code?"
        }
    },
    "filters": {
        ".eslintrc": "lint",
        ".eslintignore": "lint"
    },
    "completeMessage": "To get started:\n\n  cd {{destDirName}}\n  npm install\n  npm run mock\n  npm run dev"
};