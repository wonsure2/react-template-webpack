# {{ name }}

> {{ description }}

## Build Setup

```bash
# install dependencies
npm install
  
# start mock server at localhost:3000
# npm run mock
  
# serve with hot reload at localhost:8080
npm run dev
  
# build for production with minification
npm run build
  
# eslint code
npm run eslint
  
# test with jest
npm run jest
```
