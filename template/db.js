let faker = require('faker');
faker.locale = 'zh_CN';

const mockData = {};

mockData['apprentices'] = (function () {
    const apprentices = [];
    for (let id = 0; id < 10; id++) {
        const gender = faker.random.arrayElement([0, 1]);
        apprentices.push({
            id,
            name: faker.name.firstName(gender) + faker.name.lastName(gender),
            active: faker.random.boolean,
            gender: gender ? '男' : '女',
        })
    }
    return apprentices;
})();

module.exports = function () {
    return mockData;
};