import React, { Component } from 'react';
import Link from './components/Link';
import "whatwg-fetch"; // fetch polyfill
import * as styles from './index.less';
import * as bigImage from './images/big-image.jpg';
import * as smallImage from './images/small-image.png';

class MyApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            students: [],
        }
    }

    componentDidMount() {
        fetch('/api/apprentices')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    students: json,
                });
            })
            .catch(() => {

            });
    }

    render() {
        return (
            <div>
                <Link>link</Link>
                <img src={bigImage.default} className={styles.bigImage} alt=""/>
                <img src={smallImage.default} className={styles.smallImage} alt=""/>
                <h5>Student List</h5>
                {
                    this.state.students.map((e, i) => {
                        return (
                            <div key={i}>
                                {e.name} {e.gender}
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}

export default MyApp;