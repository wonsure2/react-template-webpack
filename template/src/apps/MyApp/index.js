import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import MyApp from './MyApp';
import './index.less';

const container = document.getElementById('app');

if (process.env.NODE_ENV === 'production') {
    ReactDOM.render(<MyApp/>, container);
} else {
    const render = (Component, container) => {
        ReactDOM.render(
            <AppContainer>
                <Component/>
            </AppContainer>,
            container
        );
    };

    render(MyApp, container);

    if (module.hot) {
        module.hot.accept('./MyApp', () => {
            const NextRootContainer = require('./MyApp').default;
            render(NextRootContainer, container);
        });
    }
}