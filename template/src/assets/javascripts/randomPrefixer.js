const cdnPrefixes = [
    '//s3.pstatp.com/toutiao/',
    '//s3a.pstatp.com/toutiao/',
    '//s3b.pstatp.com/toutiao/'
];

function formatPath(domain, resource) {
    if (domain[domain.length - 1] !== '/') {
        domain += '/';
    }
    if (resource[0] === '/') {
        resource = resource.substr(1);
    }
    return domain + resource;
}

function randomPrefixer(type, url) {
    let link;
    const randomPrefix = cdnPrefixes[Math.random() * cdnPrefixes.length | 0];
    if (type === 'css') {
        link = 'href';
        return link + '="' + formatPath(randomPrefix, url) + '"';
    } if (type === 'js') {
        link = 'src';
        return link + '="' + formatPath(randomPrefix, url) + '"';
    } else {
        return formatPath(randomPrefix, url);
    }
}

module.exports = randomPrefixer;