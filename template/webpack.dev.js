var NODE_ENV = 'development';
var path = require('path');
var webpack = require('webpack');
var glob = require('glob');
var proxy = require('./wds-proxy.config');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

process.env.NODE_ENV = NODE_ENV;

var projectName = path.parse(__dirname).name;
var outputDir = './output';

// 获取文件路径和名称
var appPaths = glob.sync('./src/apps/*');
var apps = [];
var entries = appPaths.reduce(function (prev, next) {
    var name = /\.\/src\/apps\/(.+)/.exec(next)[1];
    prev[name] = next;
    apps.push(name);
    return prev;
}, {});

// html模版文件
var htmls = [];
for (var appName in entries) {
    if (entries.hasOwnProperty(appName)) {
        var html = {
            template: entries[appName] + '/index.ejs',
            filename: 'template/' + projectName + '/' + appName + '.html',
            chunks: ['react-lib', appName],
            nodeEnv: NODE_ENV,
        };
        htmls.push(html);
    }
}
var htmlWebpackPlugins = htmls.map(function (html) {
    return new HtmlWebpackPlugin(html);
});

// 添加React热加载相关入口
for (var appName in entries) {
    if (entries.hasOwnProperty(appName)) {
        entries[appName] = [
            'react-hot-loader/patch',
            'webpack-dev-server/client?http://localhost:8080', // WebpackDevServer host and port
            'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
            entries[appName],
        ];
    }
}

// Common chunk
entries['react-lib'] = ['react', 'react-dom', 'react-hot-loader'];

module.exports = {
    entry: entries,
    devtool: 'inline-source-map',
    devServer: {
        contentBase: outputDir, // This tells webpack-dev-server to serve the files from the dist directory on localhost:8080.
        hot: true,
        publicPath: '/',
        proxy: proxy,
        setup: webpackDevServerSetup,
    },
    output: {
        filename : 'resource/' + projectName + '/js/[name].js',
        path: path.resolve(__dirname, outputDir),
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                    {{#lint}}'eslint-loader',{{/lint}}
                ],
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                minimize: true,
                            }
                        },
                        'postcss-loader',
                        'less-loader',
                    ]
                }),
            },
            {
                test: /\.(bmp|gif|jpe?g|png|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 1024,
                    name: 'resource/' + projectName + '/images/[name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(), // This plugin will cause the relative path of the module to be displayed when HMR is enabled. Suggested for use in development. But not necessary.
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'react-lib',
            minChunks: Infinity, // 不提取公共模块
            chunks: apps, // 设置子模块
        }),
        new ExtractTextPlugin('resource/' + projectName +'/css/[name].css')
    ].concat(htmlWebpackPlugins),
};


function webpackDevServerSetup(app, server) {
    app.get('/', function (req, res) {
        res.write('<!DOCTYPE html><html><head><meta charset="utf-8"/></head><body>');
        var url = this.publicPath || '/';
        var path = server.middleware.getFilenameFromUrl(this.publicPath || '/');
        var fs = server.middleware.fileSystem; // in memory

        // 用了比较trick的方法，每1秒检查一次是否已经编译成功
        function writeDirectory() {
            var fail = false;
            function writeBody(baseUrl, basePath, depth = 0) {
                try {
                    var content = fs.readdirSync(basePath); // file name array
                    if (content.length === 0 && depth === 0) {
                        setTimeout(writeDirectory.bind(this, baseUrl, basePath), 1000); // build not done
                        fail = true;
                        return;
                    }
                } catch (err) {
                    setTimeout(writeDirectory.bind(this, baseUrl, basePath), 1000); // error
                    fail = true;
                    return;
                }
                res.write('<ul>');
                content.forEach(function (item) {
                    var p = basePath + '/' + item;
                    if (fs.statSync(p).isFile()) {
                        res.write('<li><a href="');
                        res.write(baseUrl + item);
                        res.write('">');
                        res.write(item);
                        res.write('</a></li>');
                    } else {
                        res.write('<li>');
                        res.write(item);
                        writeBody(baseUrl + item + '/', p, depth + 1);
                    }
                });
                res.write('</ul>');
            }
            writeBody(url, path);
            if (!fail) {
                res.end('</body></html>');
            }
        }
        writeDirectory();
    }.bind(this)); // bind WebpackDevServer instance
}