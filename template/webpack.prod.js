var NODE_ENV = 'production';
var path = require('path');
var webpack = require('webpack');
var glob = require('glob');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ShellJs = require('shelljs');
var randomPrefixer = require('./src/assets/javascripts/randomPrefixer');

// 路径名称
var projectName = path.parse(__dirname).name;
var outputDir = './dist';

process.env.NODE_ENV = NODE_ENV;

// 删除之前生成的文件
ShellJs.rm('-rf', outputDir);

// 获取文件路径和名称
var appPaths = glob.sync('./src/apps/*');
var apps = [];
var entries = appPaths.reduce(function (prev, next) {
    var name = /\.\/src\/apps\/(.+)/.exec(next)[1];
    prev[name] = next;
    apps.push(name);
    return prev;
}, {});

// html模版文件
var htmls = [];
for (var appName in entries) {
    if (entries.hasOwnProperty(appName)) {
        var html = {
            inject: false, // 手动生产随机cdn前缀
            minify: {
                removeComments: true,
                collapseWhitespace: true,
            },
            template: entries[appName] + '/index.ejs',
            filename: 'template/' + projectName + '/' + appName + '.html',
            chunks: ['manifest', 'tools-lib', 'react-lib', appName],
            nodeEnv: NODE_ENV,
        };
        htmls.push(html);
    }
}
var htmlWebpackPlugins = htmls.map(function (html) {
    return new HtmlWebpackPlugin(html);
});

// Common chunk
entries['react-lib'] = ['react', 'react-dom'];
entries['tools-lib'] = ['whatwg-fetch'];

module.exports = {
    entry: entries,
    devtool: false,
    output: {
        filename : 'resource/' + projectName +'/js/[name].[chunkhash:8].js',
        path: path.resolve(__dirname, outputDir),
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ['babel-loader'{{#lint}}, 'eslint-loader'{{/lint}}]
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                minimize: true,
                            }
                        },
                        'postcss-loader',
                        'less-loader',
                    ]
                })
            },
            {
                test: /\.(jpeg|jpg|png|gif)$/,
                loader: 'url-loader',
                options: {
                    limit: 1024,
                    name: 'resource/' + projectName + '/images/[name].[hash:8].[ext]',
                    publicPath: randomPrefixer.bind(null, 'image'),
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest',
            minChunks: Infinity, // 不提取公共模块
            chunks: ['tools-lib', 'react-lib'].concat(apps), // 设置子模块
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'tools-lib',
            minChunks: Infinity,
            chunks: apps,
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'react-lib',
            minChunks: Infinity,
            chunks: apps,
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new InlineManifestWebpackPlugin({
            name: 'webpackManifest', // name: default value is webpackManifest, result in htmlWebpackPlugin.files[name], you can specify any other name except manifest, beacuse the name manifest haved been used by HtmlWebpackPlugin for H5 app cache manifest.
        }),
        new ExtractTextPlugin('resource/' + projectName +'/css/[name].[contenthash:8].css'),
    ].concat(htmlWebpackPlugins),
};